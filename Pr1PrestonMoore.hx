// Project 1 - Preston Moore
//
module org.crsx.hacs.samples.Pr1PrestonMoore {

/* LEXICAL ANALYSIS. */

space [ \t\n] 
      | '//' . *
      | '/*' ([^*] | '*'+ [^/])* '*/' 
      ;                                     // white space convention

token INT    | ⟨Digit⟩+ ; //Integer literals
token ID     | ⟨IdentifierStart⟩ ⟨IdentifierEnd⟩* ;  //Identifiers
token SQSTRING | "'" ([^\'^\\] | "\\'" | "\\n" | "\\t" | "\\x"⟨HexDigit⟩⟨HexDigit⟩)* "'" ;
//token DQSTRING | '"' ([^\"^\\] | '\\"' | '\\n' | '\\t' | '\\x'⟨HexDigit⟩⟨HexDigit⟩)* '"' ;

token fragment Digit  | [0-9] ;
token fragment HexDigit | [A-Fa-f0-9] ;
token fragment IdentifierStart | [A-Za-z_$] ;
token fragment IdentifierEnd | [a-zA-Z0-9_$] ;

sort Expression | ⟦ ⟨INT⟩ ⟧@35
                | ⟦ ⟨ID⟩ ⟧@35
                | sugar ⟦ (⟨Expression#⟩) ⟧@35 → Expression#
             // | ⟦ ⟨Expression@30⟩ . ⟨Expression@29⟩ ⟧@30
                | ⟦ ⟨Expression@30⟩ . ⟨ID⟩ ⟧@30
    
                | ⟦ ⟨Expression@29⟩ ( ⟨ExpressionList⟩ ) ⟧@28
        //How to deal with this correctly
                //| ⟦ ⟨Expression⟩ ( ) ⟧@28

                | ⟦ ! ⟨Expression@28⟩ ⟧@27
                | ⟦ ~ ⟨Expression@28⟩ ⟧@27
                | ⟦ - ⟨Expression@28⟩ ⟧@27
                | ⟦ + ⟨Expression@28⟩ ⟧@27

                | ⟦ ⟨Expression@26⟩ * ⟨Expression@25⟩ ⟧@26
                | ⟦ ⟨Expression@26⟩ / ⟨Expression@25⟩ ⟧@26
                | ⟦ ⟨Expression@26⟩ % ⟨Expression@25⟩ ⟧@26

                | ⟦ ⟨Expression@25⟩ + ⟨Expression@24⟩ ⟧@25
                | ⟦ ⟨Expression@25⟩ - ⟨Expression@24⟩ ⟧@25
    
                | ⟦ ⟨Expression@24⟩ < ⟨Expression@23⟩ ⟧@24
                | ⟦ ⟨Expression@24⟩ > ⟨Expression@23⟩ ⟧@24
                | ⟦ ⟨Expression@24⟩ <= ⟨Expression@23⟩ ⟧@24
                | ⟦ ⟨Expression@24⟩ >= ⟨Expression@23⟩ ⟧@24
    
                | ⟦ ⟨Expression@23⟩ == ⟨Expression@22⟩ ⟧@23
                | ⟦ ⟨Expression@23⟩ != ⟨Expression@22⟩ ⟧@23
    
                | ⟦ ⟨Expression@22⟩ & ⟨Expression@21⟩ ⟧@22
    
                | ⟦ ⟨Expression@21⟩ ^ ⟨Expression@20⟩ ⟧@21

                | ⟦ ⟨Expression@20⟩ | ⟨Expression@19⟩ ⟧@20

                | ⟦ ⟨Expression@19⟩ && ⟨Expression@18⟩ ⟧@19

                | ⟦ ⟨Expression@18⟩ || ⟨Expression@17⟩ ⟧@18

                | ⟦ ⟨Expression@17⟩ ? ⟨Expression@16⟩ : ⟨Expression@16⟩ ⟧@17
    
                | ⟦ ⟨Expression@15⟩ = ⟨Expression@16⟩ ⟧@16

                | ⟦ ⟨Expression@15⟩ += ⟨Expression@16⟩ ⟧@16
    
                | ⟦ ⟨Expression@15⟩ = { ⟨IDExpressionList⟩ } ⟧@16
                ;
                
sort ExpressionList | ⟦ ⟨Expression⟩ ⟧
                    | ⟦ ⟨Expression⟩ , ⟨ExpressionList⟩ ⟧
                    ;

sort IDExpressionList | ⟦ ⟨ID⟩ : ⟨Expression⟩ ⟧
                      | ⟦ ⟨ID⟩ : ⟨Expression⟩, ⟨IDExpressionList⟩ ⟧
                      ;

sort Type        | ⟦ boolean ⟧
                 | ⟦ number ⟧
                 | ⟦ string ⟧
                 | ⟦ void ⟧
                 | ⟦ ⟨ID⟩ ⟧
                 | ⟦ ( ⟨TList⟩ ) => ⟨Type⟩ ⟧
                 | ⟦ ( ) => ⟨Type⟩ ⟧
                 | ⟦ { ⟨ITList⟩ } ⟧
                 ;

sort ITList | ⟦ ⟨ID⟩ : ⟨Type⟩ ⟧
            | ⟦ ⟨ID⟩ : ⟨Type⟩ , ⟨ITList⟩ ⟧
            ;

sort TList | ⟦ ⟨Type⟩ ⟧
           | ⟦ ⟨Type⟩ , ⟨TList⟩ ⟧
           ;

sort Statement | ⟦ { ⟨StatementList⟩ } ⟧
               | ⟦ var ⟨ID⟩ : ⟨Type⟩ ; ⟧
               | ⟦ ⟨Expression⟩ ; ⟧
               | ⟦ ; ⟧
               | ⟦ if ( ⟨Expression⟩ ) ⟨Statement⟩ ⟧
               | ⟦ if ( ⟨Expression⟩ ) ⟨Statement⟩ else ⟨Statement⟩ ⟧
               | ⟦ while ( ⟨Expression⟩ ) ⟨Statement⟩ ⟧
               | ⟦ return ⟨Expression⟩ ; ⟧
               | ⟦ return ; ⟧
               ;

sort StatementList | ⟦ ⟨Statement⟩ ⟧
                   | ⟦ ⟨Statement⟩ ⟨StatementList⟩ ⟧
                   ; 

sort CallSignatureEntry | ⟦ ⟨ID⟩ : ⟨Type⟩ ⟧ ;

sort CallSignatureEntryList | ⟦ ⟨CallSignatureEntry⟩  ⟧ 
                            | ⟦ ⟨CallSignatureEntry⟩ , ⟨CallSignatureEntryList⟩ ⟧
                            ;

sort CallSignature | ⟦ ( ⟨CallSignatureEntryList⟩ ) : ⟨Type⟩ ⟧
                   | ⟦ ( ) : ⟨Type⟩ ⟧
                   ;

sort InterfaceMember | ⟦ ⟨ID⟩ : ⟨Type⟩ ; ⟧ 
                     | ⟦ ⟨ID⟩ ⟨CallSignature⟩ { ⟨StatementList⟩ } ; ⟧
                     | ⟦ ⟨ID⟩ ⟨CallSignature⟩ { ⟨StatementList⟩ }  ⟧ 
                     ;

sort InterfaceMemberList | ⟦ ⟨InterfaceMember⟩ ⟧
                         | ⟦ ⟨InterfaceMember⟩ ⟨InterfaceMemberList⟩ ⟧
                         ;

sort InterfaceDeclaration | ⟦ interface ⟨ID⟩ { ⟨InterfaceMemberList⟩ } ⟧ ;

sort FunctionDeclaration | ⟦ function ⟨ID⟩ ⟨CallSignature⟩ { ⟨StatementList⟩ } ; ⟧
                         | ⟦ function ⟨ID⟩ ⟨CallSignature⟩ { ⟨StatementList⟩ }  ⟧ 
                         ;

sort Declaration | ⟦ ⟨InterfaceDeclaration⟩ ⟧ 
                 | ⟦ ⟨FunctionDeclaration⟩ ⟧
                 ;

sort StatementOrDeclaration | ⟦ ⟨Statement⟩ ⟧
                            | ⟦ ⟨Declaration⟩ ⟧
                            ;

sort StatementOrDeclarationList | ⟦ ⟨StatementOrDeclaration⟩ ⟧
                                | ⟦ ⟨StatementOrDeclaration⟩ ⟨StatementOrDeclarationList⟩ ⟧
                                ;

//Program needs to require at least one statement
sort Program | ⟦ ⟨StatementOrDeclarationList⟩ ⟧
             ;
}
